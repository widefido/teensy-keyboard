#ifndef KeyboardIO_H_
#define KeyboardIO_H_
#include "Arduino.h"

//add your includes for the project KeyboardIO here
//end of add your includes here

#ifdef __cplusplus
extern "C" {
#endif
void loop();
void setup();
#ifdef __cplusplus
} // extern "C"
#endif

typedef struct {
    uint8_t  flags;
    uint16_t rawKey;
} Key;

#include "keys.h"

//Do not add code below this line
#endif /* KeyboardIO_H_ */







#define KEYMAP_QWERTY_FN { /* Generated keymap for QWERTY_FN */\
	{Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_PageUp, Key_Skip},\
	{Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip},\
	{Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_MouseBtnL, Key_Skip, Key_Skip},\
	{Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip},\
	{Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip},\
	{Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Keymap1_Momentary, Key_Skip, Key_Skip, Key_MouseBtnR, Key_Skip, Key_MouseR, Key_Skip},\
	{Key_Skip, Key_Skip, Key_MouseUp, Key_Skip, Key_PageDn, Key_Skip, Key_Skip, Key_Skip, Key_Home, Key_Skip, Key_MouseDn},\
	{Key_Skip, Key_Skip, Key_MouseL, Key_Skip, Key_End, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip, Key_Skip},\
},

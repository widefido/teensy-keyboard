/*

 Toshiba Libretto - USB Keyboard Controller built atop the Teensy 2.0
 
 +------------+--------------------------------+--+-----------------------+
 |            |     INPUTS            |  |    OUTPUTS         |
 +------------+--------------------------------+--+-----------------------+
 | PINS       |01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|
 +------------+--------------------------------+--+-----------------------+
 | TEENSY     |00|01|02|03|04|05|06|07|08|09|10|  |A0|A1|A2|A3|A4|A5|A6|A7|
 +------------+--------------------------------+--+-----------------------+
 | COMPONENTS |rr|rr|rr|rr|rr|rr|rr|rr|rr|rr|rr|  |5v|5v|5v|5v|5v|5v|5v|5v|
 | GROUNDS    |vv|vv|vv|vv|vv|vv|vv|vv|vv|vv|vv|  |dd|dd|dd|dd|dd|dd|dd|dd| 
 +------------+--------------------------------+--+-----------------------+

 The keyboard operates in a scan loop:

 - scan:
    - bring all of the outputs low
    - for each output pin (row)
     - bring row high
     - for each input pin (column)
    - read digital value, compute debounce, and set the value in the matrix state
 - send keys:
    - figure out, based on the current matrix state and keymap, which keys to press
    - send those keys out to the USB HID


 BTW, from the Teensy page:

 A brief delay may be needed between pinMode() configuring INPUT_PULLUP mode
 and digitalRead() reporting the unconnected pin as HIGH. The pullup resistor
 raises the voltage slowly, depending on capacitance of any circuitry attached,
 plus the capacitance of the pin and breadboard or wires.
 Usually delayMicroseconds(10) is plenty."

*/

#include "keyboard.h" 
#include "qwerty.h"
#include "qwerty_fn.h"

#include <stdio.h>
#include <math.h>
#include <EEPROM.h>

#define trace //Serial.printf

#define EEPROM_LAYER_LOCATION 0

#define DEBOUNCE_MICRO_DELAY 1250
#define MATRIX_PRESSED_STATE 0xFFFF
#define MATRIX_RELEASED_STATE 0x8000

#define MAX_KEYS_PRESSED 6 // maximum number of concurrent presses

#define LAYERS  2  // KEYMAPS
#define OUTPUTS 8  // ROWS
#define INPUTS  11 // COLS

// NOTE: A10 is connected to an unused port.
static const byte OUTPUT_PINS[OUTPUTS] = { A0, A1, A2, A3, A4, A5, A6, A7 };   // ROWS
static const byte INPUT_PINS[INPUTS]   = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }; // COLS 

static const Key layers[LAYERS][OUTPUTS][INPUTS] = {
    KEYMAP_QWERTY
    KEYMAP_QWERTY_FN
};

uint16_t prevMatrixState[OUTPUTS][INPUTS]; // A two-dimensional array containing our previous key matrix state (which keys are currently pressed) 0=pressed, 1=released
uint16_t matrixState[OUTPUTS][INPUTS];     // A two-dimensional array containing our key matrix state (which keys are currently pressed) 0=pressed, 1=released

uint16_t modifiersBeingPressed;
uint16_t keysBeingPressed[MAX_KEYS_PRESSED]; // A vector for the keys we are currently holding down

unsigned long lastTick = 0;
long reportingCounter = 0;

double mouseActiveForCycles = 0;
float carriedOverX = 0;
float carriedOverY = 0;

uint8_t currentLayer = 0;
int8_t currentMouseX = 0;
int8_t currentMouseY = 0;


////////////
// MATRIX //
////////////


// setup the matrix pinouts. we're using a high row pulse state with pull-down resistors on the inputs.
void prepareMatrix(){
    // outputs should be low
    for(byte i = 0; i < OUTPUTS; i++){
        pinMode(OUTPUT_PINS[i], OUTPUT);
        digitalWrite(OUTPUT_PINS[i], LOW);
    }

    // inputs should be pulled low
    for(byte i = 0; i < INPUTS; i++){
        pinMode(INPUT_PINS[i], INPUT);
        digitalWrite(INPUT_PINS[i], LOW); // remember to pull the input pins low via a 10k Ohm resistor to ground
    }

    // init the matrix state to 0
    for (byte col = 0; col < INPUTS; col++) {
        for (byte row = 0; row < OUTPUTS; row++) {
            prevMatrixState[row][col] = MATRIX_RELEASED_STATE;
            matrixState[row][col] = MATRIX_RELEASED_STATE;
        }
    } 
}


// scan the matrix for pressed keys
void scanMatrix(){
    // bring the output pin high and read the column state
    for(byte row = 0; row < OUTPUTS; row++){
        digitalWrite(OUTPUT_PINS[row], HIGH);    
        for(byte col = 0; col < INPUTS; col++){
            byte currentState = digitalRead(INPUT_PINS[col]);

            // debounce based on "An Alternative": http://www.eng.utah.edu/~cs5780/debouncing.pdf
            // the article says use !currentState, because their state is 0 if the switch is pressed
            // but ours is reverse (1 if the switch is pressed), so we can OR it as is
            // we also use the 0x8000 default state to add two additional steps to the debounce
            prevMatrixState[row][col] = matrixState[row][col];
            matrixState[row][col] = (matrixState[row][col] << 1) | currentState | MATRIX_RELEASED_STATE;
        }

        digitalWrite(OUTPUT_PINS[row], LOW);
    }
}


// use the current matrix state to send key and mouse events
void processKeyEvents(){
    bool stateChanged = false;
    bool mouseChanged = false;
    uint8_t layer = currentLayer;
    int8_t x = currentMouseX;
    int8_t y = currentMouseY;

    for (byte row = 0; row < OUTPUTS; row++) {
        for (byte col = 0; col < INPUTS; col++) {
            uint16_t switchState = matrixState[row][col];
            uint16_t prevSwitchState = prevMatrixState[row][col];
            Key mappedKey = layers[layer][row][col];

            if (switchState == MATRIX_PRESSED_STATE && prevSwitchState != MATRIX_PRESSED_STATE) {
                if(mappedKey.flags == 0 && mappedKey.rawKey == 0){
                    trace("Skip\n");
                    continue;
                }

                if((mappedKey.flags & MOMENTARY) == MOMENTARY){
                    currentLayer++;
                    layer = currentLayer;
                    continue;
                }

                if((mappedKey.flags & MOUSE_KEY) == MOUSE_KEY){
                    if(mappedKey.rawKey & MOUSE_UP) { y-=1; }
                    if(mappedKey.rawKey & MOUSE_DN) { y+=1; }
                    if(mappedKey.rawKey & MOUSE_L)  { x-=1; }
                    if(mappedKey.rawKey & MOUSE_R)  { x+=1; }
                    mouseChanged = true;
                    continue;
                }

                if((mappedKey.flags & SYNTHETIC_KEY) == SYNTHETIC_KEY){
                    switch(mappedKey.rawKey){
                    case KEY_MOUSE_BTN_L:
                    case KEY_MOUSE_BTN_M:
                    case KEY_MOUSE_BTN_R:
                        Mouse.press(mappedKey.rawKey);
                        break;
                    }
                    continue;
                }

                recordKeyPressed(mappedKey.rawKey);
                stateChanged = true;
            } else if(switchState == MATRIX_RELEASED_STATE && prevSwitchState != MATRIX_RELEASED_STATE){
                if(mappedKey.flags == 0 && mappedKey.rawKey == 0){
                    trace("Skip\n");
                    continue;
                }

                if((mappedKey.flags & MOMENTARY) == MOMENTARY){
                    currentLayer--;
                    layer = currentLayer;
                    continue;
                }

                if((mappedKey.flags & MOUSE_KEY) == MOUSE_KEY){
                    if(mappedKey.rawKey & MOUSE_UP) { y=0; }
                    if(mappedKey.rawKey & MOUSE_DN) { y=0; }
                    if(mappedKey.rawKey & MOUSE_L)  { x=0; }
                    if(mappedKey.rawKey & MOUSE_R)  { x=0; }
                    mouseChanged = true;
                    continue;
                }

                if((mappedKey.flags & SYNTHETIC_KEY) == SYNTHETIC_KEY){
                    switch(mappedKey.rawKey){
                    case KEY_MOUSE_BTN_L:
                    case KEY_MOUSE_BTN_M:
                    case KEY_MOUSE_BTN_R:
                        Mouse.release(mappedKey.rawKey);
                        break;
                    }
                    continue;
                }

                recordKeyReleased(mappedKey.rawKey);
                stateChanged = true;
            }
        }
    }

    if(stateChanged){
        trace("modifers: %u\n", modifiersBeingPressed);
        Keyboard.set_modifier(modifiersBeingPressed);

        for(byte i = 0; i < MAX_KEYS_PRESSED; i++){
            trace("key %d: %u\n", i+1, keysBeingPressed[i]);
            setKey(i, keysBeingPressed[i]);
        }

        Keyboard.send_now();
    }

    if(mouseChanged){
        trace("mouse %d %d\n", x, y);
        currentMouseX = x;
        currentMouseY = y;
    }

    handleMouseMovement(x, y);
}


// handle mouse movement events
void handleMouseMovement(int8_t x, int8_t y){
    if (x!=0 || y!=0) {
        mouseActiveForCycles++;
        double accel = computeMouseAcceleration(mouseActiveForCycles);
        float moveX = 0;
        float moveY = 0;
        if (x>0) {
            moveX = (x*accel) + carriedOverX;
            carriedOverX = moveX - floor(moveX);
        } 
        else if(x<0) {
            moveX = (x*accel) - carriedOverX;
            carriedOverX = ceil(moveX) - moveX;
        }

        if (y >0) {
            moveY = (y*accel) + carriedOverY;
            carriedOverY = moveY - floor(moveY);
        } 
        else if (y<0) {
            moveY = (y*accel) - carriedOverY;
            carriedOverY = ceil(moveY) - moveY;
        }
        Mouse.move(moveX,moveY, 0);
    } 
    else {
        mouseActiveForCycles=0;
    }
}


// convenience function for setting pressed keys with a 0-based index
void setKey(uint8_t keyNum, uint16_t key){
    switch(keyNum){
        case 0:
            Keyboard.set_key1(key);
        case 1:
            Keyboard.set_key2(key);
        case 2:
            Keyboard.set_key3(key);
        case 3:
            Keyboard.set_key4(key);
        case 4:
            Keyboard.set_key5(key);
        case 5:
            Keyboard.set_key6(key);
    }
}


// mark a key as being pressed
void recordKeyPressed(uint16_t key){
    switch(key){
        case MODIFIERKEY_CTRL:
        case MODIFIERKEY_ALT:
        case MODIFIERKEY_SHIFT:
        case MODIFIERKEY_GUI:
            modifiersBeingPressed |= key;
            break;

        default:
            for(byte i = 0; i < MAX_KEYS_PRESSED; i++){
                if(keysBeingPressed[i] == key){
                    return;
                }
            }
            
            for(byte i = 0; i < MAX_KEYS_PRESSED; i++){
                if(keysBeingPressed[i] == 0x00){
                    keysBeingPressed[i] = key;
                    break;
                }
            }
            break;
    }
}


// mark this key as being released
void recordKeyReleased(uint16_t key){
    switch(key){
        case MODIFIERKEY_CTRL:
        case MODIFIERKEY_ALT:
        case MODIFIERKEY_SHIFT:
        case MODIFIERKEY_GUI:
            modifiersBeingPressed &= ~key;
            break;

        default:
            for(byte i = 0; i < MAX_KEYS_PRESSED; i++){
                if(keysBeingPressed[i] == key){
                    keysBeingPressed[i] = 0x00;
                    return;
                }
            }
            break;
    }
}


///////////
// DEBUG //
///////////


// dump the matrix to the serial line
void dumpMatrix(){
    for(byte i = 0; i < OUTPUTS; i++){
        for(byte j = 0; j < INPUTS; j++){
            trace("%u, ", matrixState[i][j] == MATRIX_PRESSED_STATE);
        }
        trace("\n");
    }
    trace("\n");
}


// output a debug dump of the current matrix state on the serial line every 100 cycles
void reportMatrix(){
    if(reportingCounter++ % 100 == 0){
        dumpMatrix();
    }
}


//////////
// UTIL //
//////////


// output a breathing led
void breathe(){
    float val = (exp(sin(millis()/2000.0*PI)) - 0.36787944)*108.0;
    analogWrite(11, val);
}


// compute an acceleration value for mouse movement
double computeMouseAcceleration(double cycles){
    double accel = atan((cycles/50)-5);
    accel += 1.5707963267944; // we want the whole s curve, not just the bit that's usually above the x and y axes;
    accel = accel *0.85;
    if (accel<0.25) {
        accel =0.25;
    }
    return accel;
}


//////////
// MAIN //
//////////


// initialize usb and the I/O pins
void setup(){
    // prepare breathe pin
    pinMode(11, OUTPUT);

    // prepare pins
    prepareMatrix();

    // initialize USB HID
    Keyboard.begin();
    Mouse.begin();

    // delay 1 second to give the USB HID some time to connect
    delay(1000);
}


// start the scan loop
void loop(){
    // small loop delay to help with bounce
    unsigned long tick = micros();
    if(tick - lastTick < DEBOUNCE_MICRO_DELAY){
        return;
    }
    lastTick = tick;

    scanMatrix();
    //reportMatrix();
    processKeyEvents();
    //breathe();
}

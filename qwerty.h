#define KEYMAP_QWERTY { /* Generated keymap for QWERTY */\
	{Key_9, Key_O, Key_L, Key_F12, Key_Pause, Key_Skip, Key_Skip, Key_LGUI, Key_Print, Key_UpArrow, Key_Period},\
	{Key_F10, Key_F11, Key_Minus, Key_0, Key_Skip, Key_Semicolon, Key_Quote, Key_LShift, Key_Slash, Key_LBracket, Key_P},\
	{Key_F8, Key_F9, Key_I, Key_8, Key_Backtick, Key_Return, Key_Comma, Key_Skip, Key_Space, Key_RBracket, Key_K},\
	{Key_F6, Key_F7, Key_7, Key_6, Key_M, Key_J, Key_H, Key_Menu, Key_N, Key_U, Key_Y},\
	{Key_F4, Key_F5, Key_5, Key_4, Key_V, Key_F, Key_G, Key_Skip, Key_B, Key_T, Key_R},\
	{Key_F2, Key_F3, Key_Equals, Key_3, Key_Keymap1_Momentary, Key_LAlt, Key_C, Key_LCtrl, Key_Skip, Key_D, Key_E},\
	{Key_Escape, Key_F1, Key_W, Key_2, Key_DnArrow, Key_Insert, Key_Delete, Key_LShift, Key_LArrow, Key_X, Key_S},\
	{Key_Tab, Key_1, Key_A, Key_Q, Key_RArrow, Key_Backslash, Key_Skip, Key_CapsLock, Key_Skip, Key_Backspace, Key_Z},\
},

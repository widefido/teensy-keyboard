#define KEY_FLAGS       B00000000
#define CTRL_HELD       B00000001
#define ALT_HELD        B00000010
#define SHIFT_HELD      B00000100
#define GUI_HELD        B00001000
#define SWITCH_TO_LAYER B00010000
#define MOMENTARY       B00100000
#define MOUSE_KEY       B01000000
#define SYNTHETIC_KEY   B10000000

// we assert that synthetic keys can never have keys held, so we reuse the _HELD bits
#define IS_MACRO       B00000001

#define MOUSE_UP       B0001
#define MOUSE_DN     B0010
#define MOUSE_L     B0100
#define MOUSE_R    B1000

#define LAYER_0     0
#define LAYER_1     1
#define LAYER_2     2
#define LAYER_3     3
#define LAYER_4     4
#define LAYER_5     5
#define LAYER_6     6
#define LAYER_7     7

#define LAYER_PREVIOUS  253
#define LAYER_NEXT      254

#define Key_NoKey (Key){ KEY_FLAGS,0 }
#define Key_Skip (Key){ KEY_FLAGS,0 }

#define Key_MacroKey1 (Key){ KEY_FLAGS|SYNTHETIC_KEY|IS_MACRO, 1}

#define Key_MouseUpL   (Key){ KEY_FLAGS|MOUSE_KEY, MOUSE_UP | MOUSE_L }
#define Key_MouseUp    (Key){ KEY_FLAGS|MOUSE_KEY, MOUSE_UP }
#define Key_MouseUpR  (Key){ KEY_FLAGS|MOUSE_KEY, MOUSE_UP | MOUSE_R }
#define Key_MouseL    (Key){ KEY_FLAGS|MOUSE_KEY, MOUSE_L }
#define Key_MouseR    (Key){ KEY_FLAGS|MOUSE_KEY, MOUSE_R }
#define Key_MouseDnL   (Key){ KEY_FLAGS|MOUSE_KEY, MOUSE_DN | MOUSE_L  }
#define Key_MouseDn    (Key){ KEY_FLAGS|MOUSE_KEY, MOUSE_DN }
#define Key_MouseDnR    (Key){ KEY_FLAGS|MOUSE_KEY, MOUSE_DN | MOUSE_R  }
#define Key_MouseScrollUp
#define Key_MouseScrollDn
#define Key_MouseScrollL
#define Key_MouseScrollR

#define KEY_MOUSE_BTN_L 0x01 // Synthetic key
#define KEY_MOUSE_BTN_M 0x04 // Synthetic key
#define KEY_MOUSE_BTN_R 0x02 // Synthetic key
#define Key_MouseBtnL    (Key){ KEY_FLAGS | SYNTHETIC_KEY, KEY_MOUSE_BTN_L }
#define Key_MouseBtnM    (Key){ KEY_FLAGS | SYNTHETIC_KEY , KEY_MOUSE_BTN_M }
#define Key_MouseBtnR    (Key){ KEY_FLAGS | SYNTHETIC_KEY, KEY_MOUSE_BTN_R }


#define Key_LCtrl (Key){ KEY_FLAGS, KEY_LEFT_CTRL }
#define Key_LShift (Key){ KEY_FLAGS, KEY_LEFT_SHIFT }
#define Key_LAlt (Key){ KEY_FLAGS, KEY_LEFT_ALT }
#define Key_LGUI (Key){ KEY_FLAGS, KEY_LEFT_GUI }
#define Key_RCtrl (Key){ KEY_FLAGS, KEY_RIGHT_CTRL }
#define Key_RShift (Key){ KEY_FLAGS, KEY_RIGHT_SHIFT }
#define Key_RAlt (Key){ KEY_FLAGS, KEY_RIGHT_ALT }
#define Key_RGUI (Key){ KEY_FLAGS, KEY_RIGHT_GUI }

#define Key_UpArrow (Key){ KEY_FLAGS, KEY_UP_ARROW }
#define Key_DnArrow (Key){ KEY_FLAGS, KEY_DOWN_ARROW }
#define Key_LArrow (Key){ KEY_FLAGS, KEY_LEFT_ARROW }
#define Key_RArrow (Key){ KEY_FLAGS, KEY_RIGHT_ARROW }
#define Key_Return (Key){ KEY_FLAGS, KEY_RETURN }
#define Key_Escape (Key){ KEY_FLAGS, KEY_ESC }
#define Key_Backspace (Key){ KEY_FLAGS, KEY_BACKSPACE }
#define Key_Tab (Key){ KEY_FLAGS, KEY_TAB }
#define Key_Insert (Key){ KEY_FLAGS, KEY_INSERT }
#define Key_Delete (Key){ KEY_FLAGS, KEY_DELETE }
#define Key_PageUp (Key){ KEY_FLAGS, KEY_PAGE_UP }
#define Key_PageDn (Key){ KEY_FLAGS, KEY_PAGE_DOWN }
#define Key_Home (Key){ KEY_FLAGS, KEY_HOME }
#define Key_End (Key){ KEY_FLAGS, KEY_END }

#define Key_CapsLock (Key){ KEY_FLAGS, KEY_CAPS_LOCK }
#define Key_F1 (Key){ KEY_FLAGS, KEY_F1 }
#define Key_F2 (Key){ KEY_FLAGS, KEY_F2 }
#define Key_F3 (Key){ KEY_FLAGS, KEY_F3 }
#define Key_F4 (Key){ KEY_FLAGS, KEY_F4 }
#define Key_F5 (Key){ KEY_FLAGS, KEY_F5 }
#define Key_F6 (Key){ KEY_FLAGS, KEY_F6 }
#define Key_F7 (Key){ KEY_FLAGS, KEY_F7 }
#define Key_F8 (Key){ KEY_FLAGS, KEY_F8 }
#define Key_F9 (Key){ KEY_FLAGS, KEY_F9 }
#define Key_F10 (Key){ KEY_FLAGS, KEY_F10 }
#define Key_F11 (Key){ KEY_FLAGS, KEY_F11 }
#define Key_F12 (Key){ KEY_FLAGS, KEY_F12 }
#define Key_F13 (Key){ KEY_FLAGS, KEY_F13 }
#define Key_F14 (Key){ KEY_FLAGS, KEY_F14 }
#define Key_F15 (Key){ KEY_FLAGS, KEY_F15 }
#define Key_F16 (Key){ KEY_FLAGS, KEY_F16 }
#define Key_F17 (Key){ KEY_FLAGS, KEY_F17 }
#define Key_F18 (Key){ KEY_FLAGS, KEY_F18 }
#define Key_F19 (Key){ KEY_FLAGS, KEY_F19 }
#define Key_F20 (Key){ KEY_FLAGS, KEY_F20 }
#define Key_F21 (Key){ KEY_FLAGS, KEY_F21 }
#define Key_F22 (Key){ KEY_FLAGS, KEY_F22 }
#define Key_F23 (Key){ KEY_FLAGS, KEY_F23 }
#define Key_F24 (Key){ KEY_FLAGS, KEY_F24 }

#define Key_0 (Key){ KEY_FLAGS, KEY_0 }
#define Key_1 (Key){ KEY_FLAGS, KEY_1 }
#define Key_2 (Key){ KEY_FLAGS, KEY_2 }
#define Key_3 (Key){ KEY_FLAGS, KEY_3 }
#define Key_4 (Key){ KEY_FLAGS, KEY_4 }
#define Key_5 (Key){ KEY_FLAGS, KEY_5 }
#define Key_6 (Key){ KEY_FLAGS, KEY_6 }
#define Key_7 (Key){ KEY_FLAGS, KEY_7 }
#define Key_8 (Key){ KEY_FLAGS, KEY_8 }
#define Key_9 (Key){ KEY_FLAGS, KEY_9 }
#define Key_A (Key){ KEY_FLAGS, KEY_A }
#define Key_B (Key){ KEY_FLAGS, KEY_B }
#define Key_C (Key){ KEY_FLAGS, KEY_C }
#define Key_D (Key){ KEY_FLAGS, KEY_D }
#define Key_E (Key){ KEY_FLAGS, KEY_E }
#define Key_F (Key){ KEY_FLAGS, KEY_F }
#define Key_G (Key){ KEY_FLAGS, KEY_G }
#define Key_H (Key){ KEY_FLAGS, KEY_H }
#define Key_I (Key){ KEY_FLAGS, KEY_I }
#define Key_J (Key){ KEY_FLAGS, KEY_J }
#define Key_K (Key){ KEY_FLAGS, KEY_K }
#define Key_L (Key){ KEY_FLAGS, KEY_L }
#define Key_M (Key){ KEY_FLAGS, KEY_M }
#define Key_N (Key){ KEY_FLAGS, KEY_N }
#define Key_O (Key){ KEY_FLAGS, KEY_O }
#define Key_P (Key){ KEY_FLAGS, KEY_P }
#define Key_Q (Key){ KEY_FLAGS, KEY_Q }
#define Key_R (Key){ KEY_FLAGS, KEY_R }
#define Key_S (Key){ KEY_FLAGS, KEY_S }
#define Key_T (Key){ KEY_FLAGS, KEY_T }
#define Key_U (Key){ KEY_FLAGS, KEY_U }
#define Key_V (Key){ KEY_FLAGS, KEY_V }
#define Key_W (Key){ KEY_FLAGS, KEY_W }
#define Key_X (Key){ KEY_FLAGS, KEY_X }
#define Key_Y (Key){ KEY_FLAGS, KEY_Y }
#define Key_Z (Key){ KEY_FLAGS, KEY_Z }

#define Key_Backtick (Key){ KEY_FLAGS, KEY_TILDE }
#define Key_Minus (Key){ KEY_FLAGS, KEY_MINUS }
#define Key_Equals (Key){ KEY_FLAGS, KEY_EQUAL }
#define Key_LBracket (Key){ KEY_FLAGS, KEY_LEFT_BRACE }
#define Key_RBracket (Key){ KEY_FLAGS, KEY_RIGHT_BRACE }
#define Key_LSquareBracket (Key){ KEY_FLAGS, KEY_LEFT_BRACE }
#define Key_RSquareBracket (Key){ KEY_FLAGS, KEY_RIGHT_BRACE }
#define Key_Backslash (Key){ KEY_FLAGS, KEY_BACKSLASH }

#define Key_Semicolon (Key){ KEY_FLAGS, KEY_SEMICOLON }
#define Key_Quote (Key){ KEY_FLAGS, KEY_QUOTE }
#define Key_Comma (Key){ KEY_FLAGS, KEY_COMMA }
#define Key_Period (Key){ KEY_FLAGS, KEY_PERIOD }
#define Key_Space (Key){ KEY_FLAGS, KEY_SPACE }
#define Key_Slash (Key){ KEY_FLAGS, KEY_SLASH }

#define Key_Print (Key){ KEY_FLAGS, KEY_PRINTSCREEN }
#define Key_Pause (Key){ KEY_FLAGS, KEY_PAUSE }
#define Key_Menu (Key){ KEY_FLAGS, KEY_MENU }

#define Key_KeypadSlash (Key){ KEY_FLAGS, KEY_KEYPAD_SLASH }
#define Key_KeypadMultiply (Key){ KEY_FLAGS, KEY_KEYPAD_MULTIPLY }
#define Key_KeypadMinus (Key){ KEY_FLAGS, KEY_KEYPAD_MINUS }
#define Key_KeypadPlus (Key){ KEY_FLAGS, KEY_KEYPAD_PLUS }
#define Key_Enter (Key){ KEY_FLAGS, KEY_ENTER }
#define Key_Keypad1 (Key){ KEY_FLAGS, KEY_KEYPAD_1 }
#define Key_Keypad2 (Key){ KEY_FLAGS, KEY_KEYPAD_2 }
#define Key_Keypad3 (Key){ KEY_FLAGS, KEY_KEYPAD_3 }
#define Key_Keypad4 (Key){ KEY_FLAGS, KEY_KEYPAD_4 }
#define Key_Keypad5 (Key){ KEY_FLAGS, KEY_KEYPAD_5 }
#define Key_Keypad6 (Key){ KEY_FLAGS, KEY_KEYPAD_6 }
#define Key_Keypad7 (Key){ KEY_FLAGS, KEY_KEYPAD_7 }
#define Key_Keypad8 (Key){ KEY_FLAGS, KEY_KEYPAD_8 }
#define Key_Keypad9 (Key){ KEY_FLAGS, KEY_KEYPAD_9 }
#define Key_Keypad0 (Key){ KEY_FLAGS, KEY_KEYPAD_0 }
#define Key_KeypadDot (Key){ KEY_FLAGS, KEY_KEYPAD_PERIOD }

#define META_NEXT_KEYMAP_MOMENTARY 0xFF
#define Key_Keymap0 (Key){ KEY_FLAGS | SWITCH_TO_LAYER , LAYER_0 }
#define Key_Keymap1 (Key){ KEY_FLAGS | SWITCH_TO_LAYER , LAYER_1 }
#define Key_Keymap2 (Key){ KEY_FLAGS | SWITCH_TO_LAYER , LAYER_2 }
#define Key_Keymap3 (Key){ KEY_FLAGS | SWITCH_TO_LAYER , LAYER_3 }
#define Key_Keymap4 (Key){ KEY_FLAGS | SWITCH_TO_LAYER , LAYER_4 }
#define Key_Keymap5 (Key){ KEY_FLAGS | SWITCH_TO_LAYER , LAYER_5 }
#define Key_Keymap0_Momentary (Key){ KEY_FLAGS | SWITCH_TO_LAYER | MOMENTARY, LAYER_0 }
#define Key_Keymap1_Momentary (Key){ KEY_FLAGS | SWITCH_TO_LAYER | MOMENTARY, LAYER_1 }
#define Key_Keymap2_Momentary (Key){ KEY_FLAGS | SWITCH_TO_LAYER | MOMENTARY, LAYER_2 }
#define Key_Keymap3_Momentary (Key){ KEY_FLAGS | SWITCH_TO_LAYER | MOMENTARY, LAYER_3 }
#define Key_Keymap4_Momentary (Key){ KEY_FLAGS | SWITCH_TO_LAYER | MOMENTARY, LAYER_4 }
#define Key_Keymap5_Momentary (Key){ KEY_FLAGS | SWITCH_TO_LAYER | MOMENTARY, LAYER_5 }

#define Key_KeymapNext_Momentary (Key) {KEY_FLAGS | SWITCH_TO_LAYER | MOMENTARY, LAYER_NEXT }
#define Key_KeymapPrevious_Momentary (Key) {KEY_FLAGS | SWITCH_TO_LAYER | MOMENTARY, LAYER_PREVIOUS }






